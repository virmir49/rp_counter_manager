<?php

namespace RusPlanet\CounterManagerBundle\Service;


class CounterManager
{
    const COOKIE_USER_API_KEY = 'intuition_auth_token';

    protected $apiUrl;
    protected $apiVersion;

    private $authToken;

    //TODO: Temporary solution
    protected $curlErrors = null;

    public function __construct($apiUrl, $apiVersion, $kernelRootDir)
    {
        $this->apiUrl = $apiUrl;
        $this->apiVersion = $apiVersion;
        $this->kernelRootDir = $kernelRootDir;
    }

    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;

        return $this;
    }

    /**
     * @param $object
     * @param $entries
     * @return array|null
     */
    public function get($object, $entries)
    {
        $oneResult = false;
        if (isset($entries['type'])) {
            $oneResult = true;
            $entries = array($entries);
        }

        $data = $this->sendRequest(
            implode('/', [$this->apiUrl, $object, 'get']),
            OAUTH_HTTP_METHOD_GET,
            ['entries' => json_encode($entries)],
            $this->authToken
        );

        if (isset($data['success']) && $data['success'] == true) {
            $result = $data['result'];
            if ($oneResult) {
                $result = $data['result'][0];
            }
        } else {
            $result = null;
        }

        return $result;
    }

    /**
     * @param $object
     * @param $entries
     * @return array|null
     */
    public function add($object, $entries)
    {
        $oneResult = false;
        if (isset($entries['type'])) {
            $oneResult = true;
            $entries = array($entries);
        }

        $data = $this->sendRequest(
            implode('/', [$this->apiUrl, $object, 'add']),
            OAUTH_HTTP_METHOD_GET,
            ['entries' => json_encode($entries)],
            $this->authToken
        );

        if (isset($data['success']) && $data['success'] == true) {
            $result = $data['result'];
            if ($oneResult) {
                $result = $data['result'][0];
            }
        } else {
            $result = null;
        }

        return $result;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $params
     * @param string $authToken
     * @return array|bool
     */
    protected function sendRequest($url, $method, $params, $authToken = null)
    {
        $params = http_build_query($params);

        $curl = curl_init();

        if ($method == OAUTH_HTTP_METHOD_POST) {
            curl_setopt($curl, CURLOPT_POST, true);
        } else {
            $url = $url . '?' . $params;
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (!empty($params)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        if ($authToken) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["X-AUTH-TOKEN: $authToken"]);
        }

        $response = curl_exec($curl);

        $this->curlErrors = curl_error($curl);

        curl_close($curl);

        return $response ? json_decode($response, true) : false;
    }

    protected function checkValidResponse()
    {
        //TODO: Implement check functionality
    }

}